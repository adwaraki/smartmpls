# this sample uses python requests library
# to install it visit http://python-requests.org/
# download the zip file, unzip it somewhere and run 
# python setup.py install

import requests
import json
import xmltodict
import re
from collections import OrderedDict
import xml.etree.ElementTree as ET
import pymysql
import paramiko

def getDeviceList():
    devList = list()
    devidList = list()
    flag = False
    url = 'https://172.16.0.156:443/api/space/device-management/devices'
    username = "super"
    password = "juniper123"

    headers = {
             "Accept": "application/vnd.net.juniper.space.device-management.devices+xml;version=1",
             "Content-Type": "application/json",
            }

    r = requests.get(url, auth=(username, password), headers=headers, verify=False)
    decode_json = xmltodict.parse(r.text)
    #example=decode_json['devices']['device']
    #print (decode_json)
    

    for entry in (decode_json["devices"]["device"]):
        for key, value in entry.items():
            if (str(key) == '@key'):
                #print(str(key) + ":" + str(value))
                devidList.append(str(value))
                
            if(str(key) == "name"):
                devList.append(str(value))
       
        
    
    #for item in devList:
     #   print (item)                
    

    # print(example)
    # print(decode_json['devices']['device']['@key'])
    return(devList,devidList)

def getDeviceInfo():
    devList, devidList = getDeviceList() #devList contains names of devices, devIDList contains device ID nos.
    tfStats = dict()
    #print(len(devList))
    count = 0
    flag = False

    for entry,entryl in zip(devidList,devList):
        print(str(entry))
        count+=1
        index=0 
        tfindex=0
        child2=0
        
        url = 'https://172.16.0.156:443/api/space/device-management/devices/' + str(entry) + '/exec-rpc'
        username = "super"
        password = "juniper123"

        headers = {
                   "Accept": "application/vnd.net.juniper.space.device-management.rpc+xml;version=1",
                   "Content-Type": "application/vnd.net.juniper.space.device-management.rpc+xml;version=1;charset=UTF-8", 
                   }

        body = """
        <netconf>
          <rpcCommands>
            <rpcCommand>
              <![CDATA[<get-interface-information extensive/>
        ]]>
            </rpcCommand>
          </rpcCommands>
        </netconf>
        """
        r = requests.post(url, auth=(username, password), data=body, headers=headers, verify=False)
        #print(r.text)
        #decode_json = xmltodict.parse(r.text)
        decode_tree = ET.fromstring(r.text)
        #print(decode_tree)
        sub_root = ET.fromstring(decode_tree[2][0][1].text)
        
        #print(len(sub_root))
        for child in range(len(sub_root)):
            subsub_root = sub_root[child]
            for child2 in range(len(subsub_root)):
                #print(len(subsub_root))
                if ("ge-" in str(subsub_root[child2].text)):
                    #print(str(subsub_root[child2].tag))
                    flag = True
                    subsub_root = sub_root[index]
                    geif=str(subsub_root[child2].text)
                    #print(subsub_root[child2].text)
                    #if ("ge-" in subsub_root[child].text):
                    #print("found interface!!!!")
                
                     
                    
                if(flag == True and "traffic-statistics" in str(subsub_root[child2].tag)):
                    #print("gotstats")
                    #print(str(subsub_root[child2][0]))
                    flag = False
                    subtf_root = subsub_root[child2]
                    #print(subtf_root)
                    for grandchild in range(len(subtf_root)):
                        line = str(subtf_root[grandchild].tag)
                        if "}" in line:
                            param,value = line.split("}", 1)
                            #print(value + '\t' + subtf_root[grandchild].text)
                            tfStats[value]=subtf_root[grandchild].text
                            # print(subsub_root[child2].text)
                
                    for key, value in tfStats.items():
                        print(str(key) + ":" + str(value) + "pk:" + devList[devidList.index(entry)]+'-' +geif)
                        #print(devList[devidList.index(entry)])
                        insertElement((devList[devidList.index(entry)]+'-' +geif+str(key)), value, entry)
                        print("")
                        
            index+=1 
                  
        #for key, value in tfStats.items():
            #print(str(key) + ":" + str(value))
        #print(decode_tree)
        #print('\n')
        #device_tree = ET.fromstring(r.text)
        '''for entry2 in (decode_tree['netconf']['netConfReplies']['netConfReply']):
                if (isinstance(entry2, dict)):
                    print(count)
                    for key, value in entry2.items():
                        print(str(value))
                        sub_root = ET.fromstring(value.text)
                        print(sub_root.tag)
                else:
                    print('\n')
                    continue
                '''    
                #continue

    return tfStats
def connectDB():
    conn = pymysql.connect(host='172.16.0.157', port=3306, user='jboss', passwd='netscreen', db='junospace', autocommit=True)
    c = conn.cursor()
    return c
def insertElement(uuid, v, devid):
    c = connectDB()
    c.execute("insert into netStats (uuid, v, devID) values (%s,%s,%s)", (uuid, v, devid))
    
def remoteExec(ipaddr,ex_command):
    ssh = paramiko.SSHClient()
    ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    ssh.connect(ipaddr,username="group4", password="Group4")
    print("Connected SSH")
    stdin,stdout,stderr = ssh.exec_command(ex_command)
    st_type = type(stdin)
    sout = stdout.readlines()
    print(sout +"\t" + st_type)
    
    


if __name__ == "__main__":
    eg = getDeviceInfo()
    #eg = getDeviceList()
   # connectDB()
    #remoteExec("172.16.0.112","iperf -c 192.168.231.10 -t 100000 -i 2")
    






