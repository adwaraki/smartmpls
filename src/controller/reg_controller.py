'''
Created on Nov 13, 2014

@author: Abhishek
'''

from exceptions import ControlError, FrameworkError
import json
import logging.config
import math
import socket
import socketserver
import threading
import numpy
import pymysql
from utilities import watchdog_timer as wt

DB_HOST = '172.16.0.157'
DB_PORT = 3306
DB_USER = 'jboss'
DB_PASSWD = 'netscreen'
DB = 'junospace'

class NaiveController(socketserver.StreamRequestHandler):
    '''
    This class implements the controller without any path intelligence.
    This controller instances just chooses between the four advertised LSPs
    based on optimality criteria such as current path throughput, path latency
    and cost etc.
    '''

    def __init__(self, *args, **kwargs):
        '''
        Constructor initializing various components of the controller
        '''
        logging.config.fileConfig('logs/logging.conf')
        self.logger = logging.getLogger('controller')
        self.active_lsps = {}
        self.lsp_admin_status = {}
        self.traffic_matrix = numpy.zeros((8, 8))
        self.lsp_latency_info = {}
        self.lsp_euclidean_rank = {}
        self.lsp_best_segment_capacity = {}
        self.topology_edge_list = []
        self.router_list = ['NY', 'Miami', 'Tampa', 'Houston', 'Dallas',
                            'Chicago', 'LA', 'SF']
        self.bw_watchdog = wt.Watchdog(60, self._bw_watchdog_handler)
        self.latency_watchdog = wt.Watchdog(60, self._latency_watchdog_handler)
        self.lsp_addresses = {'LSP1': {'server': '192.168.231.10/32', 'client': '192.168.230.10/32'},
                              'LSP2': {'server': '192.168.231.11/32', 'client': '192.168.230.11/32'},
                              'LSP3': {'server': '192.168.231.12/32', 'client': '192.168.230.12/32'},
                              'LSP4': {'server': '192.168.231.13/32', 'client': '192.168.230.13/32'}
                              }
        self.topology_monitor_host = "localhost"
        self.topology_monitor_port = 9998
        self._init_lsp_info()
        self._get_bandwidth_info_from_db()
        self._get_latency_info_from_db()
        super().__init__(*args, **kwargs)
        
    def handle(self):
        '''
        This method handles requests from clients and processes them 
        inside this method. 
        '''
        curr_thread = threading.current_thread()
        self.logger.debug("Request being handled by : " + str(curr_thread.name))
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls
        self.data = json.loads(self.rfile.readline().decode("utf-8").strip())
        self.logger.debug("{} wrote:".format(self.client_address[0]))
        self.logger.debug(self.data)
        # Likewise, self.wfile is a file-like object used to write back
        # to the client
        if ("command" in self.data):
            if(self.data['command'] == 'GET-BEST-SEGMENT-BASED-LSP'):
                lsp_data = {}
                lsp_info = self.get_best_per_segment_lsp(None)
                lsp_data[lsp_info[0]] = lsp_info[1]
                lsp_data.update(self.lsp_addresses[lsp_info[0]])
                self.wfile.write(bytes(json.dumps(lsp_data) + "\n", "utf-8"))
            elif(self.data['command'] == 'GET-BEST-LSP'):
                lsp_data = {}
                lsp_info = self.get_best_lsp_for_transfer()
                lsp_data[lsp_info[0]] = lsp_info[1]
                lsp_data.update(self.lsp_addresses[lsp_info[0]])
                self.wfile.write(bytes(json.dumps(lsp_data) + "\n", "utf-8"))
            elif(self.data['command'] == 'GET-LSP-LIST'):
                self.wfile.write(bytes(json.dumps(self.active_lsps) + "\n", "utf-8"))
            else:
                self.logger.error("Command not identifiable")
                raise ControlError("Command not identifiable. Please check client status")
    
    def _init_lsp_info(self):
        '''
        This method initializes the LSP information
        '''
        
        self.active_lsps['LSP1'] = [('NY', 'Miami'), ('Miami', 'Dallas'), ('Dallas', 'SF')]
        # self.lsp_admin_status['LSP1'] = 'UP'
        self.active_lsps['LSP2'] = [('NY', 'Chicago'), ('Chicago', 'SF')]
        # self.lsp_admin_status['LSP2'] = 'UP'
        self.active_lsps['LSP3'] = [('NY', 'Tampa'), ('Tampa', 'Houston'), ('Houston', 'LA'), 
                                    ('LA', 'SF')]
        # self.lsp_admin_status['LSP3'] = 'UP'
        self.active_lsps['LSP4'] = [('NY', 'Tampa'), ('Tampa', 'Miami'), ('Miami', 'Chicago'), 
                                    ('Chicago', 'Dallas'), ('Dallas', 'SF')]
        # self.lsp_admin_status['LSP4'] = 'UP'
    
    def _bw_watchdog_handler(self):
        '''
        This method handles the watchdog timer expiration
        '''
        self.logger.debug("Watchdog timer expired. Fetching bandwidth data again")
        self._get_bandwidth_info_from_db()
    
    def _latency_watchdog_handler(self):
        '''
        This method handles the watchdog timer expiration for latency
        '''
        self.logger.debug("Watchdog timer expired. Fetching latency data again")
        self._get_latency_info_from_db()
                
    def _get_bandwidth_info_from_db(self):
        '''
        This method retrieves current bandwidth utilization
        on a given interface from the DB
        '''
        self.logger.debug("Connecting to junos space to retrieve stats")
        sql_stmt = """select * from junospace.netStats where 
                        uuid like "%ge%put-bps%" and ifstat = 'up'"""
        self.logger.debug("SQL : " + sql_stmt)
        try:
            db_conn = pymysql.connect(host=DB_HOST, 
                                      port=DB_PORT, 
                                      user=DB_USER, 
                                      passwd=DB_PASSWD, 
                                      db=DB)
            cursor = db_conn.cursor()
            cursor.execute(sql_stmt)
            result_set = cursor.fetchall()
            for result in result_set:
                self.logger.debug(result)
                dst, src, interface, queue, unit = result[0].split('-')
                if('bps' in unit):
                    if((dst not in self.router_list)
                       or 
                       (src not in self.router_list)):
                        continue
                    else:
                        if(queue == 'input'):
                            utilized_bw = result[1]
                        elif(queue == 'output'):
                            utilized_bw += result[1]
                            self.logger.debug("Utilized Bandwidth before conversion : " 
                                              + str(utilized_bw))
                            utilized_bw = round((utilized_bw/1000000), 3)
                            self.logger.debug("Utilized Bandwidth after conversion : " 
                                              + str(utilized_bw))
                            bw_matrix_index1 = self.router_list.index(src)
                            bw_matrix_index2 = self.router_list.index(dst)
                            self.traffic_matrix[bw_matrix_index1]\
                                [bw_matrix_index2] = utilized_bw
                            utilized_bw = 0
                            
                        else:
                            self.logger.debug("Unidentified queue")
                            raise ControlError("Unidentified queue in result set")
                else:
                    self.logger.debug("Encountered unknown measurement stat")
                    raise FrameworkError("Measurement stat bps not found. Check SQL statement")
        finally:
            db_conn.close()
            self.logger.info("Traffic Matrix : " + str(self.traffic_matrix))
            self.get_topology_edge_list()
            self.bw_watchdog.reset()
    
    def _get_latency_info_from_db(self):
        '''
        This method retrieves the latency information from
        the DB
         '''
        self.logger.debug("Connecting to junos space to retrieve stats")
        sql_stmt = """select lspName, latency, admin_status from junospace.latency"""
        self.logger.debug("SQL : " + sql_stmt)
        try:
            db_conn = pymysql.connect(host=DB_HOST, 
                                      port=DB_PORT, 
                                      user=DB_USER, 
                                      passwd=DB_PASSWD, 
                                      db=DB)
            cursor = db_conn.cursor()
            cursor.execute(sql_stmt)
            result_set = cursor.fetchall()
            for result in result_set:
                self.lsp_latency_info[str(result[0])] = (result[1]/1000)
                self.lsp_admin_status[str(result[0])] = str(result[2])
        finally:
            db_conn.close()
            self.logger.info("Latency Information : " + str(self.lsp_latency_info))
            self.latency_watchdog.reset()
    
    def _update_lsp_per_segment_availability(self):
        '''
        This method compares the LSPs against one another and decides 
        which is the best LSP on the basis of best performing segment/capacity
        '''
        for lsp_id, lsp_segments in self.active_lsps.items():
            self.logger.debug("Operating on LSP : " + str(lsp_id))
            min_segment_bw = 0 
            for segment in lsp_segments:
                self.logger.debug("LSP Segment : " + str(segment))
                segment_bw = self._calculate_available_bandwidth(segment)
                if((min_segment_bw == 0)
                   or
                   (segment_bw < min_segment_bw)):
                    min_segment_bw = segment_bw
                else:
                    continue
            if(self.lsp_admin_status[lsp_id] == 'UP'):
                self.lsp_best_segment_capacity[lsp_id] = round(min_segment_bw, 3)
            else:
                self.logger.debug(str(lsp_id) + " is not operational. Ignoring")
                continue
            
    def get_best_per_segment_lsp(self, lsp_id=None):
        
        # This updates each LSP with it's weakest per-segment available capacity
        #TODO - This has to be moved out of here and into the polling method for updation 
        self._update_lsp_per_segment_availability()
        # This returns the max(min(per-segment across all LSPs))
        self.logger.info("LSP bounding link capacity : " + str(self.lsp_best_segment_capacity))
        if(lsp_id is None):
            return (max(self.lsp_best_segment_capacity.items(), key=lambda x: x[1]))
        else:
            return(self.lsp_best_segment_capacity[lsp_id])
                
    def _calculate_available_bandwidth(self, segment=()):
        '''
        This method calculates the available bandwidth for each segment.
        The bps on a certain interface is taken into consideration with
        respect to the link capacity to infer available bandwidth on that
        segment
        '''
        
        link_capacity = 1000    # This is the capacity of each link
        node1, node2 = segment
        
        bw_matrix_index1 = self.router_list.index(node1)
        bw_matrix_index2 = self.router_list.index(node2)
        
        # Get current throughput values from bandwidth matrix
        current_if1_bps = self.traffic_matrix[bw_matrix_index1][bw_matrix_index2]
        current_if2_bps = self.traffic_matrix[bw_matrix_index2][bw_matrix_index1]
        available_bw = max((link_capacity - current_if1_bps), 
                           (link_capacity - current_if2_bps))
        self.logger.debug("Available Bandwidth : " + str(available_bw))
        return (available_bw)
    
    def _calculate_lsp_euclidean_rank(self, LSP=None, 
                                      bw_priority=2, 
                                      latency_priority=3,
                                      hc_priority=1):
        '''
        This method calculates the Euclidean rank of each LSP with respect
        to every other LSP. Once we have ranked the LSPs based on 
        available bandwidth, latency and hop count, we can then use the LSP
        with the highest rank for the next data transfer request
        '''
        weight_bw = bw_priority
        weight_latency = latency_priority
        weight_hc = hc_priority
        
        for lsp_id in self.active_lsps.keys():
            if(self.lsp_admin_status[lsp_id] == 'UP'):
                lsp_available_bw = self.get_best_per_segment_lsp(lsp_id)
                lsp_latency = self.lsp_latency_info[lsp_id]
                lsp_hop_count = len(self.active_lsps[lsp_id])
                
                rank_value = math.sqrt( ((weight_bw * lsp_available_bw)**2) +
                                        ((weight_latency * (1/lsp_latency))**2) +
                                        ((weight_hc * (1/lsp_hop_count))**2) )
                if(self.lsp_admin_status[lsp_id] == 'UP'):
                    self.lsp_euclidean_rank[lsp_id] = round(rank_value, 3)
                else:
                    self.logger.debug(str(lsp_id) + " is not operational. Ignoring")
                    continue
            
        self.logger.info("LSP Rank Info : " + str(self.lsp_euclidean_rank))
        return(max(self.lsp_euclidean_rank.items(), key=lambda x: x[1]))
    
    def get_best_lsp_for_transfer(self):
        return(self._calculate_lsp_euclidean_rank())
    
    def get_topology_edge_list(self):
        '''
        This method retrieves the topology edge list by querying the 
        topology monitor daemon
        '''
        data = {}
        data['command'] = 'GET-EDGE-LIST'

        # Create a socket (SOCK_STREAM means a TCP socket)
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # Connect to server and send data
            sock.connect((self.topology_monitor_host, 
                          self.topology_monitor_port))
            sock.sendall(bytes(json.dumps(data) + "\n", "utf-8"))

            # Receive data from the server and shut down
            self.topology_edge_list = str(sock.recv(1024), "utf-8")
        finally:
            sock.close()
        self.logger.debug("Topology Edge List :" +
                          str(self.topology_edge_list))
    
class ThreadedNaiveController(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass

if __name__ == "__main__":
    
    HOST, PORT = "localhost", 9999
    hosted_controller = ThreadedNaiveController((HOST, PORT), NaiveController)
    controller_master_thread = threading.Thread(target=hosted_controller.serve_forever())
    controller_master_thread.daemon = True
    controller_master_thread.start()
    print("Controller started in : " + controller_master_thread.name)
    