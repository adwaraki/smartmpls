# this sample uses python requests library
# to install it visit http://python-requests.org/
# download the zip file, unzip it somewhere and run 
# python setup.py install

import requests
import json
import xmltodict
from collections import defaultdict

def getDeviceList():
    url = 'https://172.16.0.156:443/api/space/device-management/devices'
    username = "super"
    password = "juniper123"

    headers = {
             "Accept": "application/vnd.net.juniper.space.device-management.devices+xml;version=1",
             "Content-Type": "application/json",
            }

    r = requests.get(url, auth=(username, password), headers=headers, verify=False)
    decode_json = xmltodict.parse(r.text)
    #example=decode_json['devices']['device']

    for entry in (decode_json["devices"]["device"]):
        for key, value in entry.items():
            print(str(key) + ":" + str(value))
    
    
    # print(example)
    # print(decode_json['devices']['device']['@key'])
    return(decode_json)

def getDeviceInfo():
    devList = getDeviceList()

        
    url = 'https://172.16.0.156:443/api/space/device-management/devices/131104/exec-rpc'
    username = "super"
    password = "juniper123"

    headers = {
  "Accept": "application/vnd.net.juniper.space.device-management.rpc+xml;version=1",
  "Content-Type": "application/vnd.net.juniper.space.device-management.rpc+xml;version=1;charset=UTF-8", 
    }

    body = """
    <netconf>
      <rpcCommands>
        <rpcCommand>
          <![CDATA[<get-interface-information extensive/>
    ]]>
        </rpcCommand>
      </rpcCommands>
    </netconf>
    """
    r = requests.post(url, auth=(username, password), data=body, headers=headers, verify=False)
    

    decode_json = xmltodict.parse(r.text)
    print (decode_json)
    return decode_json

if __name__ == "__main__":
    eg = getDeviceList()
    






