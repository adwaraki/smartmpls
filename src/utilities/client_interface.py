'''
Created on Nov 16, 2014

@author: Abhishek
'''

import socket
import sys
import json
import paramiko
from exceptions import UtilitiesError

class TextClientUI(object):
    
    def __init__(self):
        self.HOST = "localhost"
        self.vm_mgmt_ip = '172.16.0.112'
        self.command_data = {}
        self.init_ui()
        
    def init_ui(self):
        '''
        This method generates a text based interface for 
        selecting operations
        '''
        while(True):
            print("\n================= SmartMPLS Menu =================\n")
            print("\n1. Transfer data to server - Best capacity/segment\n")
            print("\n2. Transfer data to server - SmartMPLS\n")
            print("\n3. Simulate router failure\n")
            print("\n4. Simulate router recovery\n")
            print("\n5. Exit\n")
            print("\n==================================================\n")
        
            user_choice = input("\nEnter your choice : ")
            if(user_choice == '1'):
                recv_data_list = []
                received_data = self.get_best_segment_based_lsp(9999)
                recv_data_list = received_data.split(',')
                for entry in recv_data_list:
                    key, value = entry.split(':')
                    if('client' in key):
                        client_address = value.split('/')[0]
                    elif('server' in key):
                        server_address = value.split('/')[0]
                    elif('LSP' in key):
                        lsp_id = key
                        lsp_bounding_bandwidth = value
                    else:
                        continue
                print("Client Address : " + client_address)
                print("Server Address : " + server_address)
                print("LSP ID : " + lsp_id)
                print("LSP Limiting Bandwidth : " + lsp_bounding_bandwidth)
                remote_command = "iperf -c " + client_address +\
                                    " -t 100000 -i 2"
                # self.remote_exec(self.vm_mgmt_ip, remote_command)
            elif(user_choice == '2'):
                recv_data_list = []
                received_data = self.get_best_lsp(9999)
                recv_data_list = received_data.split(',')
                for entry in recv_data_list:
                    key, value = entry.split(':')
                    if('client' in key):
                        client_address = value.split('/')[0]
                    elif('server' in key):
                        server_address = value.split('/')[0]
                    elif('LSP' in key):
                        lsp_id = key
                        lsp_bounding_bandwidth = value
                    else:
                        continue
                print("Client Address : " + client_address)
                print("Server Address : " + server_address)
                print("LSP ID : " + lsp_id)
                print("LSP Rank Info : " + lsp_bounding_bandwidth)
                remote_command = "iperf -c " + client_address +\
                                    " -t 100000 -i 2"
                # self.remote_exec(self.vm_mgmt_ip, remote_command)
            elif(user_choice == '3'):
                received_data = self.simulate_router_failure(9998)
            elif(user_choice == '4'):
                received_data = self.simulate_router_recovery(9998)
            elif(user_choice == '5'):
                sys.exit(1)
            else:
                raise UtilitiesError("Incorrect choice entered")
    
    def get_best_segment_based_lsp(self, PORT=9999):
        '''
        This method sends a command to the controller to retrieve
        the best LSP based on capacity/segment
        '''
        self.command_data['command'] = 'GET-BEST-SEGMENT-BASED-LSP'
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # Connect to server and send data
            sock.connect((self.HOST, PORT))
            sock.sendall(bytes(json.dumps(self.command_data) + "\n", "utf-8"))
            # Receive data from the server and shut down
            lsp_info = str(sock.recv(1024), "utf-8")
            print("Server/client info to connect to : " +
                  str(lsp_info))
            return(lsp_info)
        finally:
            sock.close()
    
    def get_best_lsp(self, PORT=9999):
        '''
        This method retrieves sends a command to the controller to 
        retrieve the best LSP based on multiple parameters
        '''
        self.command_data['command'] = 'GET-BEST-LSP'
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # Connect to server and send data
            sock.connect((self.HOST, PORT))
            sock.sendall(bytes(json.dumps(self.command_data) + "\n", "utf-8"))
            # Receive data from the server and shut down
            lsp_info = str(sock.recv(1024), "utf-8")
            return(lsp_info)
        finally:
            sock.close()
    
    def simulate_router_failure(self, PORT=9998):
        '''
        This method retrieves sends a command to the controller to 
        retrieve the best LSP based on multiple parameters
        '''
        self.command_data['command'] = 'SIMULATE-ROUTER-FAILURE'
        self.command_data['router_name'] = 'Chicago'
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # Connect to server and send data
            sock.connect((self.HOST, PORT))
            sock.sendall(bytes(json.dumps(self.command_data) + "\n", "utf-8"))
            # Receive data from the server and shut down
            inactive_lsps = str(sock.recv(1024), "utf-8")
            print("Server/client info to connect to : " +
                  str(inactive_lsps))
            return(inactive_lsps)
        finally:
            sock.close()
    
    def simulate_router_recovery(self, PORT=9998):
        '''
        This method sends a message to the topology monitor
        to simulate a router failure.
        '''
        self.command_data['command'] = 'SIMULATE-ROUTER-RECOVERY'
        self.command_data['router_name'] = 'Chicago'
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        try:
            # Connect to server and send data
            sock.connect((self.HOST, PORT))
            sock.sendall(bytes(json.dumps(self.command_data) + "\n", "utf-8"))
            # Receive data from the server and shut down
            reactivated_lsps = str(sock.recv(1024), "utf-8")
            print("Deactivated LSPs operational again")
            return(reactivated_lsps)
        finally:
            sock.close()
            
    def remote_exec(self, remote_ip, command):
        '''
        This method executes a command remotely on a given
        IP address or hostname
        '''
        ssh = paramiko.SSHClient()
        ssh.set_missing_host_key_policy(paramiko.AutoAddPolicy())
        ssh.connect(remote_ip, username="group4", password="Group4")
        print("SSH connected successfully")
        stdin, stdout, stderr = ssh.exec_command(command)
        st_type = type(stdin)
        sout = stdout.readlines()
        print(sout +"\t" + st_type)

if __name__ == "__main__":
    ui_object = TextClientUI()