'''
Created on Nov 17, 2014

@author: Abhishek
'''

import logging.config
from threading import Timer

logging.config.fileConfig('logs/logging.conf')
logger = logging.getLogger('root')
        
class Watchdog:
    def __init__(self, timeout, userHandler=None):  # timeout in seconds
        logger.debug("Initiliazing watchdog timer. Timeout : " + str(timeout))
        self.timeout = timeout
        self.handler = userHandler if userHandler is not None else self.defaultHandler
        self.timer = Timer(self.timeout, self.handler)
        self.timer.start()

    def reset(self):
        logger.debug("Canceling timer for reset")
        self.timer.cancel()
        logger.debug("Resetting timer")
        self.timer = Timer(self.timeout, self.handler)
        self.timer.start()

    def stop(self):
        self.timer.cancel()

    def defaultHandler(self):
        raise self