#!/usr/bin/python3.3
# pylint: disable=C0301

''' Base class for undirected graphs

This class uses any hashable object in the Python data reference as a node and uses 
associated key-value pairs to describe the node

Created on Feb 4, 2014

This framework has substantial code from the NetworkX graph library (essentially a stripped
down version of the networkx lib) - Could not use the 
framework "as is" because of the requirements to modify the BFS code for our functionality. 
Any modifications to the NetworkX library meant re-writing the code anyway.

'''

# Copyright (C) 2004-2011 by
# Aric Hagberg <hagberg@lanl.gov>
# Dan Schult <dschult@colgate.edu>
# Pieter Swart <swart@lanl.gov>
# All rights reserved.
# BSD license.

import logging.config
from copy import deepcopy
from collections import defaultdict
from exceptions import FrameworkError

__author__ = """\n""".join(['Abhishek Dwaraki (adwaraki@umass.edu)'])

class Graph(object):
    """
    Base class for undirected graphs.

    A Graph stores nodes and edges with optional data, or attributes.

    Graphs hold undirected edges.  Self loops are not allowed but multiple
    (parallel) edges are not.

    Nodes can be arbitrary hashable Python objects with optional
    key/value attributes.

    Edges are represented as links between nodes with optional
    key/value attributes.
    
    Parameters
    ----------
    
    attr : list of key-value attributes for the graph 
    
    """
    
    def __init__(self, **attr):
        """ Initializes a graph with a name, node and attributes
        
        Parameters 
        ----------
        
        attr : Key-value pairs that hold graph attribute values 
        
        """
        
        self.graph_attr = {}
        self.node_list = {}
        self.adj_list = {}
        
        # Set graph attributes from list
        self.graph_attr.update(attr)
        self.edge_list = self.adj_list
        
        logging.config.fileConfig('logs/logging.conf')
        self.logger = logging.getLogger('controller')
        
    @property
    def name(self):
        return self.graph_attr.get('name','')
    @name.setter
    def name(self, graph_name):
        self.graph_attr['name'] = graph_name
        
    def __str__(self):
        """ Returns the name of the graph
        
        name : string
        
        """
        return self.name
    
    def __iter__(self):
        """ Iterate over the nodes in the graph. This returns an iterator of the
        node iterable
        
        """
        return iter(self.node_list)
    
    def __contains__(self, node):
        """ True if G contains node, False otherwise
        
        """ 
        try:
            return node in self.node_list
        except TypeError:
            return False
    
    def __len__(self):
        """ Returns the number of nodes in the Graph
        
        """
        return len(self.node_list)
    
    def __getitem__(self, node):
        """ Returns the adjacency list for the node specified. There are two ways to
        accomplish this. Either return the internal adjacency list dict or transform it
        into a list. The second method is accomplished using a class method
        
        """
        return self.adj_list[node]
    
    def add_node(self, node, attr_dict=None, **attr_list):
        """ Add the node specified to the graph 
        
        Parameters
        ----------
        
        node : node, required
            Is any hashable object that can be inserted into the internal dict with key-value
            pairs as attributes
            
        attr_dict : dictionary, optional. 
            Contains information such as node id, predecessor, node path list information, 
            node color etc. This list can expand at a later stage
            
        attr_list :  keyword list, optional
            This is a list of attributes to be added/updated in the node's internal dict
        
        """
        
        if attr_dict is None:
            attr_dict = attr_list
            attr_dict['id'] = str(node)
            attr_dict['color'] = 'White'
            attr_dict['predecessor'] = None
            attr_dict['visit_count'] = 0
            attr_dict['native_node_type'] = type(node)
            attr_dict['path_table'] = defaultdict(dict)
            attr_dict['path_table'] = defaultdict(dict)
            attr_dict['suboptimal_path_table'] = defaultdict(dict)
        else:
            try:
                attr_dict['id'] = str(node)
                attr_dict['color'] = "White"
                attr_dict['predecessor'] = None
                attr_dict['visit_count'] = 0
                attr_dict['native_node_type'] = type(node)
                attr_dict['path_table'] = defaultdict(dict)
                attr_dict['suboptimal_path_table'] = defaultdict(dict)
                attr_dict.update(attr_list)
            except AttributeError:
                raise FrameworkError("Attribute key-value pairs should be a dict")
        
        if node not in self.node_list:
            self.adj_list[node] = {}
            self.node_list[node] = attr_dict
        else:
            # Update attribute dictionary if node exists
            self.node_list[node].update(attr_dict) 
    
    def remove_node(self, node):
        """ Remove node from graph
        Removes the node n and all adjacent edges.

        Parameters
        ----------
        n : node, required
            A node in the graph

        Raises
        -------
        FrameworkError
            If n is not in the graph.
            
        """
        adj_list = self.adj_list
        try:
            neighbors = list(adj_list[node].keys())
            del self.node_list[node]
        except KeyError:
            raise FrameworkError("The node %s is not in the graph."%(node,))
        for nbr in neighbors:
            del adj_list[nbr][node] # delete all adjacency edges in the graph
        del adj_list[node]
        
    def nodes_iterator(self, iter_flag=False):
        """ Returns an iterator over the nodes in the graph
        
        Parameters
        ----------
        iter_flag : optional, default=False
            If true, returns a tuple of the node and its attribute dictionary, else 
            returns just the nodes
            
        """
        
        if iter_flag:
            return iter(self.node_list.items())
        else:
            return iter(self.node_list)
    
    def get_nodes(self):
        """ Returns the nodes of the graph
        
        """
        return(list(self.node_list))
    
    def get_node_count(self):
        """ Returns the number of nodes in the graph
        
        """
        return(len(self.node_list))
    
    def is_node_present(self, node):
        """ Checks if the given node is present in the graph
        
        Parameters
        ----------
        node : node, required
            Returns True if node in graph, else False
            
        """
        try:
            return (node in self.node_list)
        except TypeError:
            return False
    
    def add_edge(self, node1, node2, attr_dict=None, **attr_list):
        """ Adds an edge between node1 and node2 to the graph, modifying the 
        respective adjacency lists. 
        
        Parameters
        ----------
        
        node1, node2: required
            End points of edge in graph 
        attr_dict : optional, default=None
            dict of edge attributes 
        attr_list : optional
            keyword-value pairs of updateable attributes
        
        Notes :
            Edge weight is commonly used term to denote edge characteristics in graphs.
            This is a default keyword in the dict for an edge. In our cases, we use 
            'bandwidth', 'latency' and 'cost' as the basic defaults
            
        """
        self.logger.debug("Attempting to add edge between " + 
                          str(node1) + " and " + str(node2))
        if attr_dict is None:
            attr_dict = attr_list
        else:
            try:
                attr_dict.update(attr_list)
            except AttributeError:
                raise FrameworkError("Attribute list has to be a dict")
        # First add nodes if they aren't already in the list
        if node1 not in self.node_list:
            self.node_list[node1] = {}
            self.adj_list[node1] = {}
            self.node_list[node1]['id'] = str(node1)
            self.node_list[node1]['color'] = "White"
            self.node_list[node1]['predecessor'] = None
            self.node_list[node1]['visit_count'] = 0
            self.node_list[node1]['native_node_type'] = type(node1)
            self.node_list[node1]['path_table'] = defaultdict(dict)
            self.node_list[node1]['suboptimal_path_table'] = defaultdict(dict)
            self.adj_list[node1] = {}
        if node2 not in self.node_list:
            self.node_list[node2] = {}
            self.adj_list[node2] = {}
            self.node_list[node2]['id'] = str(node2)
            self.node_list[node2]['color'] = "White"
            self.node_list[node2]['predecessor'] = None
            self.node_list[node2]['visit_count'] = 0
            self.node_list[node2]['native_node_type'] = type(node2)
            self.node_list[node2]['path_table'] = defaultdict(dict)
            self.node_list[node2]['suboptimal_path_table'] = defaultdict(dict)
            self.adj_list[node2] = {}
        # Add the edge to the adjacency lists of both nodes
        edge_attr_dict = self.adj_list[node1].get(node2, {})
        edge_attr_dict.update(attr_dict)
        self.adj_list[node1][node2] = edge_attr_dict
        self.adj_list[node2][node1] = edge_attr_dict
        
    def remove_edge(self, node1, node2):
        """ Remove the edge from the graph
        
        Parameters
        ----------
        node1, node2 : Nodes, required
        
        Raises
        ------
        FrameworkError : if the edge doesn't exist
        
        """
        try:
            del self.adj_list[node1][node2]
            if node1 != node2:
                del self.adj_list[node2][node1]
        except KeyError:
            raise FrameworkError("The edge %s-%s does not exist in the graph" %(node1,node2))
    
    def is_edge_present(self, node1, node2):
        """ Checks for the existence of the edge
        
        Parameters
        ----------
        node1, node2 : required
            nodes that are edge end points
        
        Returns
        -------
        bool : boolean True if the edge exists, False otherwise
        
        """
        try:
            return node2 in self.adj_list[node1]
        except KeyError:
            return False
        
    def node_neighbors(self, node):
        """ Returns the neighbors for the node specified
        
        Parameters
        ----------
        node : required
            The node whose neighbors are queried for
        
        Returns
        -------
        node_list : list of nodes neighboring 'node'
        
        Raises
        ------
        FrameworkError : if 'node' does not exist in the graph
        
        """
        try:
            return list(self.adj_list[node])
        except KeyError:
            raise FrameworkError('The node %s does not exist in the graph' %(node,))
        
    def iter_node_neighbors(self, node):
        """ Returns the neighbors for the node specified
        
        Parameters
        ----------
        node : required
            The node whose neighbors are queried for
        
        Returns
        -------
        node_list : iterator of nodes neighboring 'node'
        
        Raises
        ------
        FrameworkError : if 'node' does not exist in the graph
        
        """
        try:
            return iter(self.adj_list[node])
        except KeyError:
            raise FrameworkError('The node %s does not exist in the graph' %(node,))
        
    def get_edge_data(self, node1, node2, default=None):
        """Return the attribute dictionary associated with edge (u,v).

        Parameters
        ----------
        node1, node2 : nodes, required
            default: any Python object (default=None)
            Value to return if the edge (u,v) is not found.
        
        Returns
        -------
        edge_dict : dictionary
            The edge attribute dictionary.

        """
        try:
            return self.adj_list[node1][node2]
        except KeyError:
            return default
    
    def adjacency_list(self):
        """Return an adjacency list representation of the graph.
    
        The output adjacency list is in the order of G.nodes().
        For directed graphs, only outgoing adjacencies are included.
        
        Returns
        -------
        adj_list : lists of lists
        The adjacency structure of the graph as a list of lists.
        
        """
        return list(map(list,iter(self.adj_list.values())))
    
    def adjacency_iter(self):
        """Return an iterator of (node, adjacency dict) tuples for all nodes.
    
        This is the fastest way to look at every edge.
        For directed graphs, only outgoing adjacencies are included.
        
        Returns
        -------
        adj_iter : iterator
        An iterator of (node, adjacency dictionary) for all nodes in
        the graph.
        
        """
        return iter(self.adj_list.items())
    
    def clear(self):
        """ Clears all the nodes and edges in the graph
        
        """
        self.name = ''
        self.node_list.clear()
        self.adj_list.clear()
        self.graph_attr.clear()
    
    def reset_nodes(self):
        """ Resets all the nodes of the graph to their
        original values for a new BFS run
        """
        for node in self.nodes_iterator():
            self.node_list[node]['color'] = "White"
            self.node_list[node]['predecessor'] = None
            self.node_list[node]['path_table'] = defaultdict(dict)
            self.node_list[node]['suboptimal_path_table'] = defaultdict(dict)
    
    def copy(self):
        """ Creates a copy of the graph to be operated upon
        """
        return deepcopy(self)