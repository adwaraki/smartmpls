'''
Created on Nov 13, 2014

@author: Abhishek
'''

import requests
import logging.config

class RestParser(object):
    '''
    This class processes a REST API call from clients and returns  
    REST responses from the server
    '''

    def __init__(self, params):
        '''
        Constructor
        '''
        logging.config.fileConfig('logs/logging.conf')
        self.logger = logging.getLogger('utils')
    
    def send_rest_request(self, url, method='GET', headers={}, body={}, auth=()):
        '''
        This method takes a rest request from a component and 
        processes it by calling the correct REST method in the 
        requests library
        '''
        username, password = auth
        try:
            if(method == 'GET'):
                self.logger.debug("Sending REST GET request")
                self.logger.debug("URL : " + url)
                self.logger.debug("REST method : " + method)
                self.logger.debug("Headers : " + str(headers))
                rest_response = requests.get(url, auth=(username, password), 
                                             headers=headers, verify=False)
            elif(method == 'POST'):
                self.logger.debug("Sending REST POST request")
                self.logger.debug("URL : " + url)
                self.logger.debug("REST method : " + method)
                self.logger.debug("Headers : " + str(headers))
                self.logger.debug("Body : " + str(body))
                rest_response = requests.post(url, auth=(username, password), 
                                              data=body, headers=headers, verify=False)
            elif(method == 'PUT'):
                self.logger.debug("Sending REST PUT request")
                rest_response = requests.put(url, headers)
            elif(method == 'HEAD'):
                self.logger.debug("Sending REST HEAD request")
                rest_response = requests.head(url, headers)
            else:
                self.logger.error("Invalid REST method")
        except ValueError:
            self.logger.error("No JSON object could be decoded. Check error codes")
        
        # Return the response as-is. Conversion takes place at the destination
        return rest_response
