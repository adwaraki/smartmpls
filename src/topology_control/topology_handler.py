'''
Created on Nov 13, 2014

@author: Abhishek
'''

import json
import logging.config
import socket
import socketserver
import threading
import pymysql
from utilities.graph import Graph
from utilities.rest_parser import RestParser as rparser
import xml.etree.ElementTree as ET
from exceptions import TopologyError

_MASTER_TOPOLOGY_GRAPH = Graph()
_TOPOLOGY_EDGE_LIST = []
inactive_lsps = {}
active_lsps = {}

class NetworkTopology(socketserver.StreamRequestHandler):
    '''
    This class handles the network topology discovery and maintenance
    '''
    
    def __init__(self, *args, **kwargs):
        '''
        Constructor
        '''
        self.GRAPH_STATUS = None
        logging.config.fileConfig('logs/logging.conf')
        self.logger = logging.getLogger('controller')
        self.router_list = ['NY', 'Miami', 'Tampa', 'Houston', 'Dallas',
                            'Chicago', 'LA', 'SF']
        self.controller_host = "localhost"
        self.controller_port = 9999
        self._init_lsp_info()
        self._build_topology_from_ifinfo()
        super().__init__(*args, **kwargs)

    @property
    def graph_status(self):
        return self.GRAPH_STATUS
    
    @graph_status.setter
    def graph_status(self, graph_status):
        self.GRAPH_STATUS = graph_status
        
    def handle(self):
        '''
        This method handles requests from clients and processes them 
        inside this method. 
        '''
        curr_thread = threading.current_thread()
        self.logger.debug("Request being handled by : " + str(curr_thread.name))
        # self.rfile is a file-like object created by the handler;
        # we can now use e.g. readline() instead of raw recv() calls
        self.data = json.loads(self.rfile.readline().decode("utf-8").strip())
        self.logger.debug("{} wrote:".format(self.client_address[0]))
        self.logger.debug(self.data)
        # Likewise, self.wfile is a file-like object used to write back
        # to the client
        if ("command" in self.data):
            if(self.data['command'] == 'GET-EDGE-LIST'):
                self.wfile.write(bytes(json.dumps(_TOPOLOGY_EDGE_LIST) + "\n", "utf-8"))
            elif(self.data['command'] == 'SIMULATE-ROUTER-FAILURE'):
                router_name = self.data['router_name']
                self._process_router_failure(router_name)
                self.wfile.write(bytes(json.dumps(inactive_lsps) + "\n", "utf-8"))
            elif(self.data['command'] == 'SIMULATE-ROUTER-RECOVERY'):
                router_name = self.data['router_name']
                self._process_router_recovery(router_name)
                self.wfile.write(bytes(json.dumps(inactive_lsps) + "\n", "utf-8"))
            else:
                self.logger.error("Command not identifiable")
                raise TopologyError("Command not identifiable. Please check client status")
    
    def _init_lsp_info(self):
        '''
        This method initializes the LSP information
        '''
        global active_lsps
        active_lsps['LSP1'] = [('NY', 'Miami'), ('Miami', 'Dallas'), ('Dallas', 'SF')]
        active_lsps['LSP2'] = [('NY', 'Chicago'), ('Chicago', 'SF')]
        active_lsps['LSP3'] = [('NY', 'Tampa'), ('Tampa', 'Houston'), ('Houston', 'LA'), 
                                    ('LA', 'SF')]
        active_lsps['LSP4'] = [('NY', 'Tampa'), ('Tampa', 'Miami'), ('Miami', 'Chicago'), 
                                    ('Chicago', 'Dallas'), ('Dallas', 'SF')]
        
    def _build_topology_from_ifinfo(self):
        device_list_url = 'https://172.16.0.156:443/api/space/device-management/devices'
        username = "super"
        password = "juniper123"

        device_list_header = {
                   "Accept": "application/vnd.net.juniper.space.device-management.devices+xml;version=1",
                   }
        
        device_info_header = {
            "Accept": "application/vnd.net.juniper.space.device-management.rpc+xml;version=1",
                   "Content-Type": "application/vnd.net.juniper.space.device-management.rpc+xml;version=1;charset=UTF-8", 
        }
        
        device_if_info_header = {
            "Accept": "application/vnd.net.juniper.space.device-management.expanded-configuration+xml;version=3",
        }

        device_info_body = """
                            <netconf>
                              <rpcCommands>
                                <rpcCommand>
                                  <![CDATA[<get-interface-information extensive/>
                                    ]]>
                                </rpcCommand>
                              </rpcCommands>
                            </netconf>
                            """
        device_list = ET.fromstring((rparser.send_rest_request(self, 
                                                               url=device_list_url, 
                                                               method='GET', 
                                                               headers=device_list_header,
                                                               auth=(username, password))).text)
        for device in device_list:
            device_id = device.attrib['key']
            device_name = device[7].text
            self.logger.debug("Device ID : " + str(device_id))
            device_info_url = 'https://172.16.0.156:443/api/space/device-management/devices/' \
                                + str(device_id) + '/exec-rpc'
            device_if_info_url =  'https://172.16.0.156:443/api/space/device-management/devices/' \
                                    + str(device_id) + '/configurations/expanded'
            device_info = ET.fromstring((rparser.send_rest_request(self, 
                                                                   url=device_if_info_url, 
                                                                   method='GET', 
                                                                   body=device_info_body, 
                                                                   headers=device_if_info_header,
                                                                   auth=(username, password))).text)
            config_data = ET.fromstring(device_info[0].text)
            for child in config_data:
                if("interfaces" in child.tag):
                    for element in child.iterfind('{http://xml.juniper.net/xnm/1.1/xnm}interface'):
                        if_name = element[0].text
                        if("ge-" in if_name):
                            self.logger.debug(str(device_name) + "-" + str(if_name) + 
                                              " is connected to " + str(element[1].text))
                            if((str(device_name) in self.router_list)
                               and 
                               (str(element[1].text) in self.router_list)):
                                _TOPOLOGY_EDGE_LIST.append(tuple(((str(device_name)), 
                                                                  str(element[1].text))))
                                _MASTER_TOPOLOGY_GRAPH.add_edge(str(device_name), 
                                                                str(element[1].text), 
                                                                endpoint1=str(if_name))
                            else:
                                continue
            self.logger.debug(str(_MASTER_TOPOLOGY_GRAPH.get_node_count()))
            self.logger.debug(str(len(_TOPOLOGY_EDGE_LIST)))
            
    def _process_router_failure(self, router_name=None):
        '''
        This method simulates a router failure and removes the corresponding LSPs
        from the list
        '''
        global active_lsps
        global inactive_lsps
        temp_lsp_list = {}
        self.logger.debug("Processing router failure. Deactivating respective LSPs")
        if(router_name == None):
            self.logger.info("Cannot simulate router failure without router name")
        else:
            for lsp_id, lsp_info in active_lsps.items():
                for segment in lsp_info:
                    if(router_name in segment):
                        inactive_lsps.update({key:active_lsps[key] for 
                                                   key in active_lsps if lsp_id in key})
            
            temp_lsp_list = {key:active_lsps[key] for 
                             key in active_lsps if key not in inactive_lsps}
            active_lsps = temp_lsp_list
            self.logger.info("Active LSP List : " + str(active_lsps))
            self.logger.info("Inactive LSP List : " + str(inactive_lsps))
        
        self.logger.debug("Connecting to junos space to update inactive LSP info")
        db_conn = pymysql.connect(host='172.16.0.157', 
                                      port=3306, 
                                      user='jboss', 
                                      passwd='netscreen', 
                                      db='junospace')
        cursor = db_conn.cursor()
        try:
            for key in inactive_lsps.keys():
                cursor.execute("""UPDATE junospace.latency SET admin_status='DOWN' 
                                WHERE lspName=%(id)s""", {'id':key})
                db_conn.commit()
        except:
            raise TopologyError("LSP info update was not successful")
        finally:
            db_conn.close()
    
    def _process_router_recovery(self, router_name=None):
        '''
        This method simulates a router recovery and adds the corresponding LSPs
        back to the active list
        '''
        global active_lsps
        global inactive_lsps
        temp_lsp_id_list = []
        self.logger.debug("Processing router recovery. Reactivating respective LSPs")
        if(router_name == None):
            self.logger.info("Cannot simulate router failure without router name")
        else:
            for lsp_id, lsp_info in inactive_lsps.items():
                self.logger.debug("LSP ID : " + str(lsp_id))
                for segment in lsp_info:
                    self.logger.debug("Segment ID : " + str(segment))
                    #TODO - There may be other routers down in the same LSP. 
                    # Need to put in checks for the same
                    if(router_name in segment):
                        if(str(lsp_id) in temp_lsp_id_list):
                            continue
                        else:
                            temp_lsp_id_list.append(str(lsp_id))
                        self.logger.debug("Partial reactivate list : " 
                                          + str(temp_lsp_id_list))
                        active_lsps[lsp_id] = lsp_info
            self.logger.debug("Pre-Inactive LSP List : " + str(inactive_lsps))
            for entry in temp_lsp_id_list:
                del(inactive_lsps[entry])
            self.logger.info("Active LSP List : " + str(active_lsps))
            self.logger.info("Inactive LSP List : " + str(inactive_lsps))
        
        self.logger.debug("Connecting to junos space to reactivate LSP info")
        self.logger.debug("LSP reactivate list : " + str(temp_lsp_id_list))
        db_conn = pymysql.connect(host='172.16.0.157', 
                                      port=3306, 
                                      user='jboss', 
                                      passwd='netscreen', 
                                      db='junospace')
        cursor = db_conn.cursor()
        try:
            for key in temp_lsp_id_list:
                cursor.execute("""UPDATE junospace.latency SET admin_status='UP' 
                                WHERE lspName=%(id)s""", {'id':key})
                db_conn.commit()
        except:
            raise TopologyError("LSP info update was not successful")
        finally:
            db_conn.close()
            
class ThreadedTopologyBuilder(socketserver.ThreadingMixIn, socketserver.TCPServer):
    pass
                     
if __name__ == "__main__":
    HOST, PORT = "localhost", 9998
    topology_server = ThreadedTopologyBuilder((HOST, PORT), NetworkTopology)
    topology_builder_main_thread = threading.Thread(target=topology_server.serve_forever())
    topology_builder_main_thread.daemon = True
    topology_builder_main_thread.start()
    print("Topology Monitor started in : " + topology_builder_main_thread.name)