# -*- coding: utf-8 -*-

"""
Created on Feb 4, 2014

----------
EXCEPTIONS
----------

Exception framework for the controller framework
 
"""

# Copyright (C) 2004-2011 by
# Aric Hagberg <hagberg@lanl.gov>
# Dan Schult <dschult@colgate.edu>
# Pieter Swart <swart@lanl.gov>
# All rights reserved.
# BSD license.

__author__="""Abhishek Dwaraki (adwaraki@umass.edu)"""

# Base framework ideas taken from the NetworkX library

# Root Exception
class FrameworkException(Exception):
    """ Base exception class """

class FrameworkError(FrameworkException):
    """ Framework error """

class ControlError(FrameworkException):
    """ Control Plane Error """
    
class TopologyError(FrameworkException):
    """ Topology Control Error """

class UtilitiesError(FrameworkException):
    """ Utilities Package Exception """